package com.array;

public class TwoDim {

	public static void main(String[] args) {
		// ref excel sheet  in below two dimension example
		int arr[][]= {{1,2},{6,7,8,9},{3,4,5}};
	
        for (int i = 0; i < arr.length; i++) {
        	//arr.length= 3(no.of floors)
			for (int j = 0; j < arr[i].length; j++) {
				System.out.println(arr[i][j]);
			}
		}
	}
}
/*
 i    j   out
 0    0    1
 0    1    2
 
 1    0    6
 1    1    7
 1    2    8
 1    3    9
 
 2    0    3
 2    1    4
 2    2    5
 */



