package com.collection;

import java.util.ArrayList;

public class Arraylist {

	public static void main(String[] args) {
		
		ArrayList<String> list = new ArrayList<String>();
        
		System.out.println(list.size());
		
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		
		// Element will be stored in consecutive memory location
		System.out.println(list.size());
	
	   // Replace the second Element with B1
		
		list.set(1, "B1");
		System.out.println(list);
		
		// Remove the Fourth Element From Array list
		
		list.remove(3);// index is 3
		System.out.println(list);
		
		// Remove the value by their Char
		
		list.remove("d");
		System.out.println(list);
	}
}
