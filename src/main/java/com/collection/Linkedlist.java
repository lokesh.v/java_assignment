package com.collection;

import java.util.LinkedList;

public class Linkedlist {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 
		LinkedList<String> li= new LinkedList<String>();
		
		li.add("a");
		li.add("b");
		li.add("c");
		li.add("d");
		li.add("e");
		
		System.out.println(li);
		
		// add a1 after a
		li.add(1, "a1");
		
		System.out.println(li);
		
		//add g in first place
		
		li.addFirst("g");
		
		System.out.println(li);
		
	}

}

// a points to b , b points to c, c points to d , d points to e

//a points to a1 , a1 points b , b points to c, c points to d , d points to e