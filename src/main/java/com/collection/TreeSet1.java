package com.collection;

import java.util.TreeSet;


public class TreeSet1 {
	public static void main(String[] args) {
		  TreeSet<String> t = new TreeSet<>();
		  t.add("Hello");
	      t.add("World");
	      t.add("hi!");
	      System.out.println("Tree Set is " + t);
	      System.out.println("First Value " + t.first());
	      System.out.println("Last Value " + t.last());
	}
}
