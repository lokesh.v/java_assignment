package com.exercise;

public class Oops_Concept {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		One a=new One();

		a.display();

		Two b=new Two();

		b.display();

		System.out.println(b.add(4,2));

		System.out.println(b.add(5.,2.)); //polymorphism

		EncapTest encap = new EncapTest();

		encap.setName("Sandeep's");

		System.out.print("Name : " + encap.getName() );

		FourWheeler test = new Honda();

		test.run();


	}

}
