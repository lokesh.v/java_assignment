package com.strings;

public class StringBuffer2 {
  
	public static void main(String[] args) {
		StringBuffer s= new StringBuffer("Hello");
		// Add world next to hello
		s.append("World");
		
		System.out.println(s);
		// insert java in first index position
		s.insert(1,"Java");
		
			System.out.println(s);
			
			s.reverse();
			System.out.println(s);
	}
}
