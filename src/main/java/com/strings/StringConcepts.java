package com.strings;

public class StringConcepts {
public static void main(String[] args) {
	

	String s1 ="Welcome"; //String constant pool
	
	String s2="Welcome";//String constant pool
	
	char[] c= {'s','e','l','e','n','i','u','m'};
	
	String s3= new String("Welcome_s3"); // stored in heap memory
	
	String s4= new String("Hello_s4"); // stored in heap memory
	
	String s5= new String(c); // stored in heap memory
	
	
	System.out.println(s1);
	System.out.println(s2);
	System.out.println(s3);
	System.out.println(s4);
	System.out.println(s5);
	           
	              //equal method
	
	// equals only on content no problem where its stored in String constant pool (OR) stored in heap memory
	
		boolean a = s1.equals(s2); //true
		boolean b = s1.equals(s3); //false
		boolean d = s1.equals(s4); //false
		
		System.out.println(a);
		System.out.println(b);
		System.out.println(d);
	
		// double equal
   
	//double equal method will check the reference where its stored if the reference location not same it became false		
		boolean a2 = s1==s2; //true
		boolean b2 = s1==s3; //false
		boolean d2 = s1==s4; //false
		
	// String is immutable
	
	s1.concat("Hello World"); // we are not change the reference
	System.out.println(s1);
	
	s1=s1.concat("Java");  // we are concat(change) on the referce s1
	System.out.println(s1);
	
	
}
}