package com.demo;

public abstract class AbstractPro {

	// An abstract class is class which consists of both abstract and non abstract methods
	// Non abstract method is not compulsory ,it can have all methods as abstract method
	// An abstract method is a method without a body , it is just declared ( contains a declaration )
	// Abstract can't be instantiated , that is we can't create object for that class
	// A child class can implement all the abstract methods present in the parent class
	// what happens if a child does not implement those abstract methods
	
	public abstract void employee(); // Abstract Method
	 
	 public void department() // non Abstract Method 
	 {	 
	 }
}
